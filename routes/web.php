<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(isset(auth()->user()->id) == true){
        return redirect()->route('reservasi.index');
    }else{
        return redirect()->route('login');
    }
});

Route::get('/template',function(){
    return view('temp.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route list reservasi
Route::get('reservasi','ReservasiController@index')->name('reservasi.index');

Route::get("reservasi_pasien","ReservasiController@create")->name("reservasi.create");
