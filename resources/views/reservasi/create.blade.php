@extends('temp.index')
@section('temp.title',"Buat reservasi")

@section('content')
    <div id="app">
        <reservasi-pasien type="create"></reservasi-pasien>
    </div>
@endsection

@push('appjs')
    <script src="{{ asset('js/app.js') }}"></script>
@endpush