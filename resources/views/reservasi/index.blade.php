@extends('temp.index')
@section("temp.title","Reservasi")

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Reservasi</h1>
        </div>
        <div class="col-sm-6">
         
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar reservasi hari ini &amp;</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body" id="app">
                <index-reservasi></index-reservasi>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col -->
    </div>
  </section>
@endsection
@push('appjs')
<script src="{{ asset('js/app.js') }}"></script>
@endpush
