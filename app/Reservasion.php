<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservasion extends Model
{
    public function poli(){
        return $this->belongsTo(Poli::class);
    }
}
