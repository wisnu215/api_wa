<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservasion;
use DB;
use Carbon\Carbon;
use App\Poli;
use Log;
class ReservasiController extends Controller
{
    public function reservasi_index(){
        try{
            $reservasi = Reservasion::with('poli')->whereDate('created_at','=',Carbon::today())->orderBy('created_at','ASC')->get();
            return response()->json([
                'status'    => 'ok',
                'results'   => array(
                    'reservasi' => $reservasi
                )
            ],200);
        }catch(\Exception $e){
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
                'results'   => array(
                    'line'  => $e->getLine()
                )
            ],500);
        }
    }

    public function poli_index(){
        try{
            return response()->json([
                'status'    => 'ok',
                'data'      => Poli::all()
            ],200);
        }catch(\exception $e){
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
                'line'      => $e->getLine()
            ],500);
        }
    }

    public function store(Request $request){
        DB::beginTransaction();
        try{
            //variable antrian setting 0
            //$queue = 0;
            //Dicari poli dulu di hari ini           
            
            $poli = Poli::where('name',strtolower($request->poli))->first();
            if($poli == null){
                $p = Poli::all();
                $pol = "";
                foreach($p as $key=>$value){
                    $pol = $value->name.",".$pol;
                }
                // Log::info($p);
                return response()->json([
                    'status' => 'error',
                    'message'  => array(
                        "note"  => "Silahkan pilih poli dengan benar, untuk saat ini poli yang tersedia adalah :".$pol
                    )
                ],200);
            }
            
            $come = Carbon::now()->addMinute(10);
            $rev = Reservasion::orderBy("come","DESC")->whereDate('created_at',Carbon::today())->where("poli_id",$poli->id)->first();
            $revToday = Reservasion::orderBy("come","DESC")->whereDate('created_at',Carbon::today())->first();

            $queue = str_pad(1, 4, '0', STR_PAD_LEFT);
            if($rev != null){
                $come = Carbon::parse($rev->come)->addMinute(5);
                $queue = str_pad(($rev->queue + 1), 4, '0', STR_PAD_LEFT);
            }

            if($revToday != null){
                $queue = str_pad(($revToday->queue + 1), 4, '0', STR_PAD_LEFT);
            }
            

            $rv             = new Reservasion;
            $rv->name       = $request->name;
            $rv->gender     = $request->gender;
            $rv->phone      = $request->phone;
            $rv->poli_id    = $poli == null ? "0" : $poli->id;
            $rv->queue      = $queue;
            $rv->come       = $come->format('d-m-Y H:i');
            $rv->age        = $request->age;
            $rv->save();
            DB::commit();
            return response()->json([
                'status'    => 'ok',
                'message'   => $rv
            ],200);
        }catch(\Exception $e){
            Log::info($e->getMessage());
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
                'line'      => $e->getLine()
            ],500);
        }
    }
}
