<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservasion;
class ReservasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        return view('reservasi.index');
    }

    public function create(){
        return view('reservasi.create');
    }
}
